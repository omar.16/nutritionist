import React, { useState } from "react";


export const LoginContext = React.createContext(null);

const LoginProvider = ({ children }) => {
    const [user, setUser] = useState(null);
    return <LoginContext.Provider value={{ user, setUser }}>
        {children}
    </LoginContext.Provider>
}

export default LoginProvider;