import React, { useState } from "react";


export const DataContext = React.createContext(null)
export const bgColors = { HOME: 'white', LOGIN: '#cce8ff34', NEW_DIET: '#83c5bf08', MANAGE_FOOD: '#a1b5d808', PROGRAMS: '#d88c9a08' }

const DataProvider = ({ children }) => {
    const [bgColor, setBgColor] = useState('white');
    document.querySelector(`body`).style.backgroundColor = bgColor
    return <DataContext.Provider value={{ setBgColor }}>
        {children}
    </DataContext.Provider>
}

export default DataProvider