import { useSearchParams } from "react-router-dom";


const useParams = (key) => {
    const [params, setParams] = useSearchParams();

    const setParam = (value) => {
        if (value == '') {
            params.delete(key)
        } else {
            params.set(key, value)
        }
        setParams(params);
    };

    const getParam = () => {
        const value = params.get(key);
        console.log(`value:`, value)
        return value || ''
    }

    const compare = (str) => str.toLowerCase().includes(getParam().toLowerCase());

    return [getParam(), setParam, compare]
}

export default useParams;