import { useState } from "react"

const useToggle = ({ initialState }) => {
    const [state, setState] = useState(initialState);

    const toggleState = () => {
        setState(!state);
    }
    return [state, toggleState, setState]
}

export default useToggle;