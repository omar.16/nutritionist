

const getMealInfo = (mealId) => {
    const mealList = JSON.parse(localStorage.getItem(`food-list`)) || [];
    return mealList.find(meal => meal.id == mealId);
}

const calculateTotalCalories = (patient) => {
    let totalCalories = 0;
    if (patient == null || patient.weeklyMeals == null) return 0;
    const weeklyMeals = patient.weeklyMeals
    for (let i = 0; i < 7; i++) {
        if (weeklyMeals[i] == null) continue;
        for (let j = 0; j < weeklyMeals[i].length; j++) {
            totalCalories += parseInt(weeklyMeals[i][j].amount * getMealInfo(weeklyMeals[i][j].mealId).calories);
        }
    }
    return totalCalories;
}

export { getMealInfo, calculateTotalCalories }