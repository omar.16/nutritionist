

const getPageCount = ({ itemsNum, currentPageNum, itemsPerPage }) => {
    const totalPages = Math.ceil(itemsNum / itemsPerPage)
    const itemPos = (currentPageNum * itemsPerPage)
    return { totalPages, itemPos }
}

export default getPageCount;