
const getAuthenticated = ({ username, password }) => {
    const users = JSON.parse(localStorage.getItem(`users`)) || [];
    return users.find(user => (user.username == username && user.password == password));
}

const addNewUser = ({ username, password, img }) => {
    const users = JSON.parse(localStorage.getItem(`users`)) || [];
    users.push({ username, password, img })
    localStorage.setItem(`users`, JSON.stringify(users))
}

const userExists = ({ username }) => {
    const users = JSON.parse(localStorage.getItem(`users`)) || [];
    return users.find(user => user.username == username);
}

export { addNewUser, getAuthenticated, userExists }