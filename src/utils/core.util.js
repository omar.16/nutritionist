import { v4 as uuidv4 } from 'uuid';

const generateRandomUUID = () => uuidv4();

export default generateRandomUUID