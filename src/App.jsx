import "./App.css";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import React, { useState } from "react";
import Header from "./components/common/header/header.component";
import NotificationProvider from "./components/common/notification/notification-container/notification-container.component";
import HomePage from "./pages/home/home.component";
import ManageFoodPage from "./pages/manage-food/manage-food.component";
import ProgramsPage from "./pages/programs/manage-food.component";
import NewDietPage from "./pages/new-diet/new-diet.component";
import ShowTimer from "./components/common/show-timer/show-timer.component";
import LoginPage from "./pages/login/login.component";
import LoginProvider from "./providers/login.provider";
import DataProvider from "./providers/data.provider";

const App = () => {

  // return <BasicDocument></BasicDocument>

  return (
    <div className="App">
      <DataProvider>
        <LoginProvider>
          <BrowserRouter>
            <NotificationProvider>
              <Header></Header>
              <Routes>
                <Route path="/" element={<Navigate to="/home" replace />} />
                <Route path="/home" element={<HomePage />} />
                <Route path="/new-diet" element={<NewDietPage />} />
                <Route path="/manage-food" element={<ShowTimer timeout={0}><ManageFoodPage className="manage-food-page hidable" /></ShowTimer>} />
                <Route path="/programs" element={<ProgramsPage />} />
                <Route path="/login" element={<LoginPage />} />
              </Routes>
            </NotificationProvider>
          </BrowserRouter>
        </LoginProvider>
      </DataProvider>
    </div>
  );
};

export default App;
