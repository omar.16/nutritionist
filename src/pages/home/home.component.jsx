import './home.css'
import CardButton from '../../components/home/card-button/card-button.component';
import ShowTimer from '../../components/common/show-timer/show-timer.component';
import { useNavigate } from 'react-router-dom';
import { useContext } from 'react';
import { bgColors, DataContext } from '../../providers/data.provider';

const HomePage = () => {
    const { setBgColor } = useContext(DataContext)
    setBgColor(bgColors.HOME)
    const navigate = useNavigate()
    return <div className="home-page">
        <div className="buttons-container">
            <ShowTimer timeout={0}><CardButton onClick={() => {navigate('/new-diet')}} color={`#83c5be`}>New Diet Program</CardButton></ShowTimer>
            <ShowTimer timeout={100}><CardButton onClick={() => {navigate('/manage-food')}} color={`#a1b5d8`}>Manage Food Table</CardButton></ShowTimer>
            <ShowTimer timeout={200}><CardButton onClick={() => {navigate('/programs')}} color={`#d88c9a`}>View Existing Programs</CardButton></ShowTimer>
        </div>
    </div>
}

export default HomePage;