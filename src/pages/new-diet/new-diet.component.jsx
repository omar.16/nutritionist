import './new-diet.css'
import InfoForm from '../../components/new-diet/info-form/info-form.component';
import Tabs from '../../components/new-diet/tabs/tabs.component';
import NewDietModal from '../../components/new-diet/new-diet-modal/new-diet-modal.component';
import { useContext, useState } from 'react';
import ShowTimer from '../../components/common/show-timer/show-timer.component';
import { bgColors, DataContext } from '../../providers/data.provider';
import generateRandomUUID from '../../utils/core.util';
import { useNavigate } from 'react-router-dom';

const NewDietPage = () => {
    const { setBgColor } = useContext(DataContext)
    setBgColor(bgColors.NEW_DIET)    

    const [modal, setModal] = useState(false)
    const [personalInfo, setPersonalInfo] = useState({ name: ``, phone: ``, email: ``, dob: ``, city: `` })
    const [weeklyMeals, setWeeklyMeals] = useState([null, null, null, null, null, null, null])
    const [editedWeekNum, setEditedWeekNum] = useState(null)

    const navigate = useNavigate()

    const updatePersonalInfo = ({ name, phone, email, dob, city }) => {
        console.log(`update: ${name}`)
        const cPersonalInfo = { ...personalInfo }
        if (name != null) cPersonalInfo.name = name;
        if (phone != null) cPersonalInfo.phone = phone;
        if (email != null) cPersonalInfo.email = email;
        if (dob != null) cPersonalInfo.dob = dob;
        if (city != null) cPersonalInfo.city = city;
        setPersonalInfo(cPersonalInfo)
    }

    const addWeeklyMeal = (weekNum, mealId, amount) => { // weekNum: [0-6]
        const cWeeklyMeals = [...weeklyMeals];
        const week = cWeeklyMeals[weekNum] || [];
        week.push({ mealId, amount });
        cWeeklyMeals[weekNum] = week;
        console.log(`cweeklymeal:`, cWeeklyMeals)
        setWeeklyMeals(cWeeklyMeals)
    }

    const savePatientData = () => {
        const patients = JSON.parse(localStorage.getItem(`patients`)) || [];
        patients.push({ id: generateRandomUUID(), name: personalInfo.name, phone: personalInfo.phone, email: personalInfo.email, dob: personalInfo.dob, city: personalInfo.city, weeklyMeals })
        localStorage.setItem(`patients`, JSON.stringify(patients))
        navigate(`/programs`)
    }

    const resetData = () => {
        setPersonalInfo({ name: ``, phone: ``, email: ``, dob: ``, city: `` })
        setWeeklyMeals([null, null, null, null, null, null, null])
    }

    return <ShowTimer timeout={0}>
        <div className="new-diet-page hidable">
            {modal && <NewDietModal setModal={setModal} addWeeklyMeal={addWeeklyMeal} editedWeekNum={editedWeekNum} setEditedWeekNum={setEditedWeekNum}></NewDietModal>}
            <InfoForm personalInfo={personalInfo} updatePersonalInfo={updatePersonalInfo}></InfoForm>
            <Tabs setModal={setModal} weeklyMeals={weeklyMeals} setWeeklyMeals={setWeeklyMeals} setEditedWeekNum={setEditedWeekNum} savePatientData={savePatientData} resetData={resetData}></Tabs>
        </div>
    </ShowTimer>
}

export default NewDietPage;