import "./login.css";

import { Navigate, useNavigate } from "react-router-dom";
import { useContext } from "react";
import { LoginContext } from "../../providers/login.provider";
import LoginForm from "../../components/login/login-form/login-form.component";
import { bgColors, DataContext } from "../../providers/data.provider";
import ShowTimer from "../../components/common/show-timer/show-timer.component";

const LoginPage = () => {
    const { setBgColor } = useContext(DataContext)
    setBgColor(bgColors.LOGIN)
    const { user } = useContext(LoginContext);
    const navigate = useNavigate();
    if (user != null) navigate(`/home`)
    return (
        <ShowTimer timeout={0}>
            <div className="login-page hidable">
                {user == null ? <LoginForm></LoginForm> : <Navigate to="/home" replace />}
            </div>
        </ShowTimer>
    );
};

export default LoginPage;