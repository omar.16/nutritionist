import './manage-food.css'
import ProgramsTable from '../../components/programs/programs-table/programs-table.component';
import { useContext } from 'react';
import { bgColors, DataContext } from '../../providers/data.provider';

const ProgramsPage = () => {
    const { setBgColor } = useContext(DataContext)
    setBgColor(bgColors.PROGRAMS)
    return <div className="manage-food-page">
        <ProgramsTable></ProgramsTable>
    </div>
}

export default ProgramsPage;