import './manage-food.css'
import FoodTable from '../../components/manage-food/food-table/food-table.component';
import { useContext } from 'react';
import { bgColors, DataContext } from '../../providers/data.provider';

const ManageFoodPage = ({ ...props }) => {
    const { setBgColor } = useContext(DataContext)
    setBgColor(bgColors.MANAGE_FOOD)
    return <div {...props}>
        <FoodTable></FoodTable>
    </div>
}

export default ManageFoodPage;