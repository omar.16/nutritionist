import './info-form.css'
import Input from '../../common/input/input.component';

const InfoForm = ({ personalInfo, updatePersonalInfo }) => {
    return <div className="info-form">
        <Input required value={personalInfo.name} onChange={(e) => { updatePersonalInfo({ name: e.target.value }) }} placeholder='John Doe' style={{ width: '180px' }} label='Name'></Input>
        <Input required type={`number`} value={personalInfo.phone} onChange={(e) => { updatePersonalInfo({ phone: e.target.value }) }} placeholder='+1234567890' style={{ width: '150px' }} label='Phone'></Input>
        <Input type={`email`} value={personalInfo.email} onChange={(e) => { updatePersonalInfo({ email: e.target.value }) }} placeholder='email@example.com' style={{ width: '180px' }} label='Email'></Input>
        <Input required type={`date`} value={personalInfo.dob} onChange={(e) => { updatePersonalInfo({ dob: e.target.value }) }} placeholder='1/1/1970' style={{ width: '250px' }} label='DOB'></Input>
        <Input required value={personalInfo.city} onChange={(e) => { updatePersonalInfo({ city: e.target.value }) }} placeholder='Hebron' style={{ width: '250px' }} label='City'></Input>
    </div>
}

export default InfoForm;