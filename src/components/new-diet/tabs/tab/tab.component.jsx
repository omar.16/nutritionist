import './tab.css'

const Tab = ({selected, children, ...props}) => {
    return <div {...props} className={`tab${selected? ` selected`: ``}`}>
        {children}
    </div>
}

export default Tab;