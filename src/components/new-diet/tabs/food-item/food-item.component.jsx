import './food-item.css'

const FoodItem = ({ name, amount, calories, img, ...props }) => {
    return <div {...props} className="food-item-container">
        <div className="img-container">
            <img src={img} alt="Food Item" />
        </div>
        <div className="info">
            <p className='title'>{name}</p>
            <p className='text'>Amount: {amount} gm</p>
            <p className='text'>Calories: {calories}</p>
        </div>
    </div>
}

export default FoodItem;