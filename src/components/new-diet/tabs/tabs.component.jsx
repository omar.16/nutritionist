import './tabs.css'
import Tab from './tab/tab.component';
import FoodItem from './food-item/food-item.component';
import { useState } from 'react';
import Button from '../../common/button/button.component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import NoResult from '../../common/no-result/no-result.component';

const Tabs = ({ setModal, weeklyMeals, setWeeklyMeals, setEditedWeekNum, savePatientData, resetData }) => {
    const [selected, setSelected] = useState(0)

    const getFoodData = (foodId) => {
        if (foodId == null) return null;
        const foodList = JSON.parse(localStorage.getItem(`food-list`)) || []
        return foodList.find((food) => food.id == foodId);
    }

    return <div className="tabs-container">
        <div className="tabs">
            <Tab selected={selected == 0} onClick={() => { setSelected(0) }}>Saturday</Tab>
            <Tab selected={selected == 1} onClick={() => { setSelected(1) }}>Sunday</Tab>
            <Tab selected={selected == 2} onClick={() => { setSelected(2) }}>Monday</Tab>
            <Tab selected={selected == 3} onClick={() => { setSelected(3) }}>Tuesday</Tab>
            <Tab selected={selected == 4} onClick={() => { setSelected(4) }}>Wednesday</Tab>
            <Tab selected={selected == 5} onClick={() => { setSelected(5) }}>Thursday</Tab>
            <Tab selected={selected == 6} onClick={() => { setSelected(6) }}>Friday</Tab>
        </div>
        <div className="body hidable">
            {
                weeklyMeals[selected] != null ?
                    weeklyMeals[selected]?.map((meal) => {
                        const foodData = getFoodData(meal?.mealId)
                        return <FoodItem key={meal?.mealId} name={foodData.name} amount={meal.amount} calories={foodData.calories} img={foodData.img}></FoodItem>
                    })
                    :
                    <div className="no-result-container">
                        <NoResult text="No Meals Found"></NoResult>
                        <Button onClick={() => { setModal(true); setEditedWeekNum(selected) }}>Add New Meal</Button>
                    </div>
            }
            {weeklyMeals[selected] != null &&
                <div onClick={() => { setModal(true); setEditedWeekNum(selected) }} className='add-meal'>
                    <FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>
                </div>}
        </div>
        <div className="footer">
            <Button onClick={resetData}>Reset</Button>
            <Button onClick={savePatientData} className={`save`}>Save</Button>
        </div>
    </div>
}

export default Tabs;