import './new-diet-modal.css'
import { useState } from 'react'
import Input from '../../common/input/input.component'
import Select from '../../common/select/select.component'
import ShowTimer from '../../common/show-timer/show-timer.component'
import Button from '../../common/button/button.component'

const NewDietModal = ({ setModal, addWeeklyMeal, editedWeekNum, setEditedWeekNum }) => {
    const [category, setCategory] = useState(``)
    const [amount, setAmount] = useState(``)

    const foodList = JSON.parse(localStorage.getItem(`food-list`)) || [];

    return <ShowTimer timeout={0}>
        <div className="modal-container hidable">
            <div className='modal'>
                <div className="header">
                    <p className='title'>Add Meal</p>
                    <p className='text'>This dialog will add a new meal to a patient record</p>
                </div>
                <div className="body">
                    <Select name="category" label="Category" onChange={(e) => {
                        setCategory(e.target.value)
                    }} required>
                        <option value="none">None</option>
                        {
                            foodList.map((food) => <option key={food.id} value={food.id}>{food.name}</option>)
                        }
                    </Select>
                    <Input type={`number`} label="Amount" placeholder='100' value={amount} onChange={(e) => { setAmount(e.target.value) }}></Input>
                </div>
                <div className="footer">
                    <Button onClick={() => {
                        document.querySelector(`.modal-container.hidable`).classList.add(`hidden`)
                        setTimeout(() => {
                            setModal(false)
                        }, 300);
                    }}>Cancel</Button>
                    <Button onClick={() => {
                        document.querySelector(`.modal-container.hidable`).classList.add(`hidden`)
                        setTimeout(() => {
                            setModal(false)
                            addWeeklyMeal(editedWeekNum, category, amount)
                            setEditedWeekNum(null)
                        }, 300);
                    }}>Save</Button>
                </div>
            </div>
        </div>
    </ShowTimer >
}

export default NewDietModal