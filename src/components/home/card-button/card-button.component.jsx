import './card-button.css'

const CardButton = ({ children, color, className, ...props }) => {
    return <div {...props} className={`card-button ${className}`} style={{ backgroundColor: color }}>
        <p className='title'>{children}</p>
        <p className='text'>View Page <span className='arrow'>»</span></p>
    </div>
}

export default CardButton