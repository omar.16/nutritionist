import './login-form.css'
import Input from '../../common/input/input.component';
import Button from '../../common/button/button.component';
import { useContext, useState } from 'react';
import useToggle from '../../../hooks/toggle.hook';
import { addNewUser, getAuthenticated, setAuthenticated, userExists } from '../../../utils/login.util';
import { LoginContext } from '../../../providers/login.provider';
import { NotificationContext } from '../../common/notification/notification-container/notification-container.component';
import { NotificationType } from '../../common/notification/notification-body/notification-body.component';

const LoginForm = () => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [isRegisterLayout, toggleRegisterLayout] = useToggle(false)
    const { setUser } = useContext(LoginContext)
    const { pushNotification } = useContext(NotificationContext)

    const submitHandler = (e) => {
        e.preventDefault();

        if (isRegisterLayout) {
            if (!userExists({ username })) {
                addNewUser({ username, password })
                setUser({ username, password, img: null })
                pushNotification(NotificationType.Success, `You have created a new account`)
                return
            }
            pushNotification(NotificationType.Failed, `Username is already in use`)
        } else {
            const user = getAuthenticated({ username, password })
            if (user == null) {
                pushNotification(NotificationType.Failed, `Wrong username or password`)
                return
            }
            setUser(user)
            pushNotification(NotificationType.Success, `You have signed in to your account`)
        }
    }

    return <form className="login-form" onSubmit={submitHandler}>
        <h1>Agent {isRegisterLayout ? `Register` : `Login`}</h1>
        <h2>Hey, Enter your details to {isRegisterLayout ? `sign up a new account` : `sign in to your account`}</h2>
        <div className='content'>
            <Input onChange={(e) => setUsername(e.target.value)} value={username} label='Username' placeholder='john_doe'></Input>
            <Input onChange={(e) => setPassword(e.target.value)} value={password} label='Password' placeholder='********' type={`password`}></Input>
            <p className='align-left'>Forgot Password?</p>
            <Button type={`submit`}>{isRegisterLayout ? `Sign Up` : `Sign In`}</Button>
            {
                isRegisterLayout ?
                    <p>Already have an account <span onClick={toggleRegisterLayout}>Sign In</span></p>
                    : <p>Don't have an account? <span onClick={toggleRegisterLayout}>Sign Up</span></p>
            }
        </div>
    </form>
}

export default LoginForm;