import './button.css'
const Button = ({ className, children, ...props }) => {
    return <button {...props} className={`button ${className}`}>
        {children}
    </button>
}

export default Button