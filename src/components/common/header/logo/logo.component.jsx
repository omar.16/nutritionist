import { Link } from 'react-router-dom';
import './logo.css'

const Logo = () => {
    return <Link to="/home" className='logo-container'>
        <p><span className='v'>N</span>utro</p>
    </Link>
}

export default Logo;