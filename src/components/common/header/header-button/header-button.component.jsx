import { Link } from 'react-router-dom'
import './header-button.css'

const HeaderButton = ({children, className, ...props}) => {
    return <Link {...props} className={`header-button ${className}`}>
        {children}
    </Link>
}

export default HeaderButton