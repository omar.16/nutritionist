import "./header.css";
import HeaderButton from "./header-button/header-button.component";
import { Link, useLocation } from "react-router-dom";
import { useContext } from "react";
import { NotificationContext } from "../notification/notification-container/notification-container.component";
import { NotificationType } from "../notification/notification-body/notification-body.component";
import Logo from "./logo/logo.component";
import { faStethoscope } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { LoginContext } from "../../../providers/login.provider";

const Header = () => {
  const location = useLocation();
  const pathname = location.pathname;
  const { user, setUser } = useContext(LoginContext);
  const { pushNotification } = useContext(NotificationContext)

  const logout = () => {
    pushNotification(NotificationType.Notice, `You have logged out`)
    setUser(null)
  }

  return (
    <div className="header-container">

      <div className="left-nav">
        <Logo></Logo>
        <HeaderButton className={pathname.includes(`/home`) ? `selected` : ``} to="/home">Home</HeaderButton>
        <HeaderButton className={pathname.includes(`/faq`) ? `selected` : ``} to="/faq">FAQs</HeaderButton>
        <HeaderButton className={pathname.includes(`/about`) ? `selected` : ``} to="/about">About us</HeaderButton>
      </div>

      <div className="right-nav">
        {/* {user && <div className='user-info'>
          <img src="https://www.nicepng.com/png/full/137-1379898_anonymous-headshot-icon-user-png.png" alt="User Image" />
          <p>{user.fullName}</p>
        </div>} */}

        {user == null ?
          <HeaderButton className={pathname.includes(`/login`) ? `selected` : ``} to="/login">Sign In</HeaderButton> :
          <HeaderButton to="/login" onClick={logout}>Logout</HeaderButton>}

        <HeaderButton className={pathname.includes(`/vote`) ? `selected ` : ` inverted`} to="/vote"><FontAwesomeIcon icon={faStethoscope} fontSize={18} color="white" /> Join Now!</HeaderButton>
      </div>

    </div>
  );
};

export default Header;