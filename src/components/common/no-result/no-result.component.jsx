import Button from '../button/button.component';
import './no-result.css'

const NoResult = ({text}) => {
    return <div className="no-result-container">
        <img className='no-result' src='https://homebond.ae/assets/images/no-result.png'></img>
        <p>{text}</p>
    </div>
}

export default NoResult;