import './notification-container.css'
import Notification, { NotificationType } from '../notification-body/notification-body.component';
import React, { useState } from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';

export const NotificationContext = React.createContext(null)

const NotificationProvider = ({ children }) => {
    const [currentNotifications, setCurrentNotifications] = useState([]);

    const pushNotification = (type, content) => {
        const id = Math.floor(Math.random() * 10000).toString();
        document.querySelector(`.notification-container`).style.opacity = 0;
        setTimeout(() => {
            setCurrentNotifications((prev) => {
                return [...prev, { id, content, type }];
            })
            document.querySelector(`.notification-container`).style.opacity = 1;
        }, 200);
    }

    const removeNotification = (id) => {
        document.querySelector(`.notification-container`).style.opacity = 0;
        setTimeout(() => {
            document.querySelector(`.notification-container`).style.opacity = 1;
            setCurrentNotifications((prev) => {
                return prev.filter(notification => notification.id != id)
            })
        }, 200);
    }

    return <NotificationContext.Provider value={{ pushNotification, removeNotification }}>
        <div className='notification-container'>
            {
                currentNotifications.map(notification => {
                    setTimeout(() => {
                        removeNotification(notification.id)
                    }, 10 * 1000);
                    return <Notification key={notification.id} type={notification.type} content={notification.content} removeNotification={() => { removeNotification(notification.id) }} />
                })
            }
        </div>
        {children}
    </NotificationContext.Provider>;
}

export default NotificationProvider;