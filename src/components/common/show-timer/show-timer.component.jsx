import React from 'react';
import useTimer from '../../../custom-hooks/timer.component';
import './show-timer.css'

const ShowTimer = ({ children, timeout }) => {
    const shown = useTimer({ timeout })
    return React.cloneElement(children, { className: `${children.props.className}${!shown ? ` hidden` : ``}` })
}

export default ShowTimer;