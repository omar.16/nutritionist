import './programs-table.css'
import { useEffect, useState } from 'react';
import SearchBar from '../../common/search/search-bar.component';
import PageCounter from '../../common/page-counter/page-counter.component';
import Button from '../../common/button/button.component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilePdf, faPenToSquare, faPlus, faXmark } from '@fortawesome/free-solid-svg-icons';
import getPageCount from '../../../utils/page.hook';
import { Navigate, useNavigate } from 'react-router-dom';
import useParams from '../../../custom-hooks/params.component';
import ProgramsPDF, { downloadPdf } from '../programs-pdf/programs-pdf.component';
import { calculateTotalCalories } from '../../../utils/meal.util';

const ProgramsTable = () => {

    const [search, setSearch, compareSearch] = useParams('search')
    const [currentPageNum, setCurrentPageNum] = useState(0)

    const navigate = useNavigate();

    const [programsList, setProgramsList] = useState([])
    const filteredProgramsList = programsList.filter((patient) => compareSearch(patient.name))

    const fetchProgramsList = () => {
        const fetchedProgramsList = JSON.parse(localStorage.getItem(`patients`)) || []
        setProgramsList(fetchedProgramsList)
    }

    const { totalPages, itemPos } = getPageCount({ itemsNum: programsList.length, currentPageNum, itemsPerPage: 5 })
    console.log(`itempos`, itemPos)

    useEffect(() => {
        fetchProgramsList();
    }, [])

    const removeProgram = (id) => {
        let fl = [...programsList];
        fl = fl.filter(programs => programs.id != id)
        localStorage.setItem(`patients`, JSON.stringify(fl));
        fetchProgramsList();
    }

    const [currentPatient, setCurrentPatient] = useState(null)
    console.log(`wm`, currentPatient)

    return <div className='food-table-container programs'>
        <ProgramsPDF currentPatient={currentPatient}></ProgramsPDF>
        <div className='header'>
            <SearchBar search={search} setSearch={setSearch}></SearchBar>
            <Button onClick={() => { navigate("/new-diet") }}><FontAwesomeIcon icon={faPlus} fontSize={14} color="white" /> Add New</Button>
            {/* <button onClick={() => generatePatientPDF("Ahmad", ['wqewqe', 'asdsad'])}>Download PDF</button> */}

        </div>
        <div className='table-container'>
            <table>
                <thead>
                    <tr>
                        <td><input type="checkbox" /></td>
                        <td>Patient</td>
                        <td>Total Calories</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    {filteredProgramsList.map((patient, i) => {
                        return (i >= itemPos && i < itemPos + 5) && <tr key={`${patient.id}${i}`}>
                            <td><input type="checkbox" /></td>
                            <td><p>{patient.name}</p></td>
                            <td><p>{calculateTotalCalories(patient)}</p></td>
                            <td><button onClick={() => { removeProgram(patient.id) }}><FontAwesomeIcon icon={faXmark} /></button><button onClick={() => {
                                setCurrentPatient(patient)
                                setTimeout(() => {
                                    downloadPdf()
                                }, 100);
                            }}><FontAwesomeIcon icon={faFilePdf} /></button></td>
                        </tr>
                    })}
                </tbody>
            </table>
        </div>
        <div className='footer'>
            <PageCounter currentPageNum={currentPageNum} setCurrentPageNum={setCurrentPageNum} totalPages={totalPages}></PageCounter>
        </div>
    </div>
}

export default ProgramsTable;