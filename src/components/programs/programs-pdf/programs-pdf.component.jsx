import './programs-pdf.css'
import React, { useState } from 'react';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import getPageCount from '../../../utils/page.hook';
import { calculateTotalCalories, getMealInfo } from '../../../utils/meal.util';

export const downloadPdf = () => {
    const input = document.getElementById('pdf-content');
    html2canvas(input)
        .then((canvas) => {
            let imgWidth = 208;
            let imgHeight = canvas.height * imgWidth / canvas.width;
            const imgData = canvas.toDataURL('img/png');
            const pdf = new jsPDF('p', 'mm', 'a4');
            pdf.addImage(imgData, 'PNG', 0, 0, imgWidth, imgHeight);
            pdf.save("download.pdf");
        })
};

const ProgramsPDF = ({ currentPatient }) => {
    const days = [`Sunday`, `Monday`, `Tuesday`, `Wednesday`, `Thursday`, `Friday`, `Saterday`]
    return <div className='pdf-container'>
        <div id="pdf-content">
            <div className='header'>
                <h1>PROGRAM VIEW</h1>
                <p>This document shows all the diet information related to {currentPatient?.name}'s program</p>
            </div>
            <div className='body'>
                <h1 className='sub-title'>Patient Information</h1>
                <div className='patient-information'>
                    <div>
                        <p>Full Name</p>
                        <p>{currentPatient?.name}</p>
                    </div>
                    <div>
                        <p>Phone Number</p>
                        <p>{currentPatient?.phone}</p>
                    </div>
                    <div>
                        <p>Email</p>
                        <p>{currentPatient?.email}</p>
                    </div>
                    <div>
                        <p>City</p>
                        <p>{currentPatient?.city}</p>
                    </div>
                    <div>
                        <p>Weekly Calories</p>
                        <p>{calculateTotalCalories(currentPatient)}</p>
                    </div>
                </div>
                <h1 className='sub-title'>Weekly Meals</h1>
                <div className='weekly-meals'>
                    {currentPatient && currentPatient.weeklyMeals.map((day, i) => {
                        let dailyCalories = 0;
                        if (day == null) return null;
                        return <div className="info-container">
                            <p className='note'>📅 {days[i]}</p>
                            <div className='table-container'>
                                <table>
                                    <thead>
                                        <tr>
                                            <td>Meal</td>
                                            <td>Calories</td>
                                            <td>Amount</td>
                                            <td>Total Calories</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {day.map((meal) => {
                                            const { name, calories } = getMealInfo(meal.mealId);
                                            dailyCalories += calories * meal.amount;
                                            return <tr>
                                                <td><p>{name}</p></td>
                                                <td><p>{calories}</p></td>
                                                <td><p>{meal.amount}</p></td>
                                                <td><p>{calories * meal.amount}</p></td>
                                            </tr>
                                        })}
                                    </tbody>
                                </table>
                            </div>
                            <p className='note'>Total calories in {days[i]}: {dailyCalories}</p>
                        </div>

                    })}
                </div>
            </div>
        </div>
    </div>
}

export default ProgramsPDF;