import './food-table.css'
import { useEffect, useState } from 'react';
import SearchBar from '../../common/search/search-bar.component';
import PageCounter from '../../common/page-counter/page-counter.component';
import Button from '../../common/button/button.component';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPenToSquare, faPlus, faXmark } from '@fortawesome/free-solid-svg-icons';
import Modal from '../modal/modal.component';
import getPageCount from '../../../utils/page.hook';
import useParams from '../../../custom-hooks/params.component';

const FoodTable = () => {
    const [search, setSearch, compareSearch] = useParams('search')
    const [currentPageNum, setCurrentPageNum] = useState(0)
    const [modal, setModal] = useState(false)

    const [foodList, setFoodList] = useState([])
    const [editedFood, setEditedFood] = useState(null)
    const filteredFoodList = foodList.filter((food) => compareSearch(food.name))

    const fetchFoodList = () => {
        const fetchedFoodList = JSON.parse(localStorage.getItem(`food-list`)) || []
        setFoodList(fetchedFoodList)
    }

    const { totalPages, itemPos } = getPageCount({ itemsNum: foodList.length, currentPageNum, itemsPerPage: 5 })

    useEffect(() => {
        fetchFoodList();
    }, [])

    const removeFood = (id) => {
        let fl = [...foodList];
        fl = fl.filter(food => food.id != id)
        localStorage.setItem(`food-list`, JSON.stringify(fl));
        fetchFoodList();
    }

    return <div className='food-table-container'>
        {(modal || editedFood) && <Modal setModal={setModal} fetchFoodList={fetchFoodList} food={editedFood} setEditedFood={setEditedFood}></Modal>}
        <div className='header'>
            <SearchBar search={search} setSearch={setSearch}></SearchBar>
            <Button onClick={() => { setModal(true) }}><FontAwesomeIcon icon={faPlus} fontSize={14} color="white" /> Add New</Button>
        </div>
        <div className='table-container'>
            <table>
                <thead>
                    <tr>
                        <td><input type="checkbox" /></td>
                        <td>Food</td>
                        <td>Image</td>
                        <td>Amount (g/ml)</td>
                        <td>Calories</td>
                        <td>Actions</td>
                    </tr>
                </thead>
                <tbody>
                    {filteredFoodList.map((food, i) => {
                        return (i >= itemPos && i < itemPos + 5) && <tr key={`${food.id}${i}`}>
                            <td><input type="checkbox" /></td>
                            <td><p>{food.name}</p></td>
                            <td><img src={JSON.parse(food.img)} alt="" /></td>
                            <td><p>{food.amount}</p></td>
                            <td><p>{food.calories}</p></td>
                            <td><button onClick={() => { removeFood(food.id) }}><FontAwesomeIcon icon={faXmark} /></button><button onClick={() => setEditedFood(foodList.find((cfood) => cfood.id == food.id))}><FontAwesomeIcon icon={faPenToSquare} /></button></td>
                        </tr>
                    })}
                </tbody>
            </table>
        </div>
        <div className='footer'>
            <PageCounter currentPageNum={currentPageNum} setCurrentPageNum={setCurrentPageNum} totalPages={totalPages}></PageCounter>
        </div>
    </div>
}

export default FoodTable;