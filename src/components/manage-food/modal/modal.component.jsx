import './modal.css'
import Input from '../../common/input/input.component'
import Button from '../../common/button/button.component'
import ShowTimer from '../../common/show-timer/show-timer.component'
import { useState } from 'react'
import generateRandomUUID from '../../../utils/core.util'

const Modal = ({ setModal, fetchFoodList, food, setEditedFood }) => {
    const [name, setName] = useState(food ? food.name : ``)
    const [img, setImg] = useState(food ? food.img : ``)
    const [amount, setAmount] = useState(food ? food.amount : ``)
    const [calories, setCalories] = useState(food ? food.calories : ``)

    const addFood = () => {
        let foodList = JSON.parse(localStorage.getItem(`food-list`)) || [];
        foodList.push({ id: generateRandomUUID(), name, img, amount, calories })
        localStorage.setItem(`food-list`, JSON.stringify(foodList));
        fetchFoodList();
    }

    const setFood = () => {
        let foodList = JSON.parse(localStorage.getItem(`food-list`)) || [];
        foodList = foodList.filter(cfood => cfood.id != food.id)
        foodList.push({ id: food.id, name, img, amount, calories })
        localStorage.setItem(`food-list`, JSON.stringify(foodList));
        setEditedFood(null);
        fetchFoodList();
    }
    return <ShowTimer timeout={0}>
        <div className="modal-container hidable">
            <div className='modal'>
                <div className="header">
                    <p className='title'>Add Food</p>
                    <p className='text'>This dialog will required some fields to create a new food listing</p>
                </div>
                <div className="body">
                    <Input label="Name" placeholder='Rice' value={name} onChange={(e) => { setName(e.target.value) }}></Input>
                    <Input label="Image URL" type={`file`} placeholder='images.google.com/' onChange={(e) => {
                        setImg(JSON.stringify(URL.createObjectURL(e.target.files[0])))
                    }}></Input>
                    <Input type={`number`} label="Amount" placeholder='100' value={amount} onChange={(e) => { setAmount(e.target.value) }}></Input>
                    <Input type={`number`} label="Calories" placeholder='356.3' value={calories} onChange={(e) => { setCalories(e.target.value) }}></Input>
                </div>
                <div className="footer">
                    <Button onClick={() => {
                        document.querySelector(`.modal-container.hidable`).classList.add(`hidden`)
                        setTimeout(() => {
                            setModal(false)
                        }, 300);
                    }}>Cancel</Button>
                    <Button onClick={() => {
                        document.querySelector(`.modal-container.hidable`).classList.add(`hidden`)
                        setTimeout(() => {
                            setModal(false)
                        }, 300);
                        (food ? setFood() : addFood())
                    }}>Save</Button>
                </div>
            </div>
        </div>
    </ShowTimer >
}

export default Modal